# rehh

R package to search for footprints of selection in population genetic data

Population genetic data such as 'Single Nucleotide Polymorphisms' (SNPs) is often used to identify genomic regions
that have been under recent natural or artificial selection and might provide clues about the molecular mechanisms of adaptation.
One approach, the concept of 'Extended Haplotype Homozygosity' (EHH), introduced by (Sabeti 2002) https://doi.org:10.1038/nature01140, has given rise to several statistics designed for whole genome scans.
The package provides functions to compute three of these, namely: 'iHS' (Voight 2006) https://doi.org:10.1371/journal.pbio.0040072 for
detecting positive or "Darwinian" selection within a single population as well as 'Rsb' (Tang 2007) https://doi.org:10.1371/journal.pbio.0050171 and
'XP-EHH' (Sabeti 2007) https://doi.org:10.1038/nature06250, targeted at differential selection between two populations.
Various plotting functions are also included to facilitate visualization and interpretation of these statistics.

The package has been described in

Gautier M. and Vitalis R. rehh: An R package to detect footprints of selection in genome-wide SNP data from haplotype structure. Bioinformatics, 2012, 28(8), 1176-1177. https://doi.org/10.1093/bioinformatics/bts115

Gautier M., Klassmann A., and Vitalis R. rehh 2.0:  a reimplementation of the R package rehh to detect positive selection from haplotype structure. Molecular Ecology Resources 2017(1), 78-90. https://doi.org/10.1111/1755-0998.12634

Klassmann A, Gautier M. Detecting selection using extended haplotype homozygosity (EHH)-based statistics in unphased or unpolarized data. PLoS One. 2022 Jan 18;17(1):e0262024. https://doi.org/10.1371/journal.pone.0262024

### Installation

Released packages can be installed from CRAN using
```{r}
install.packages("rehh")
```

The current version of the repository can be installed with help of the R-package [devtools](https://cran.r-project.org/package=devtools) by
```{r}
devtools::install_gitlab("oneoverx/rehh")
```
However, in this case the vignettes are not built.

### Usage

For information on usage please have a look at the [CRAN page of the package](https://cran.r-project.org/package=rehh), in particular the two vignettes, which both contain explanations of the statistics involved as well as an introduction to the functionality of the package.
